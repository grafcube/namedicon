const std = @import("std");
const print = std.debug.print;

const cgtk = @cImport(@cInclude("gtk/gtk.h"));

pub fn main() anyerror!void {
    const app = cgtk.gtk_application_new("org.codeberg.Grafcube.namedicon", cgtk.G_APPLICATION_FLAGS_NONE) orelse @panic("null app");
    defer cgtk.g_object_unref(app);

    _ = cgtk.g_signal_connect_data(
        app,
        "activate",
        @ptrCast(&activate),
        null,
        null,
        cgtk.G_CONNECT_AFTER,
    );
    _ = cgtk.g_application_run(@ptrCast(app), 0, null);
}

fn activate(_: *cgtk.GtkApplication) void {
    const stdout = std.io.getStdOut().writer();

    const icon_name = if (std.os.argv.len > 1)
        std.os.argv[1]
    else {
        print(
            \\No icon requested
            \\
            \\Usage: namedicon <icon-name> [<size>]
            \\  <icon-name>     : The name of the icon you want.
            \\  <size> Optional : The size of the icon you want. Defaults to 32.
            \\
            \\Only the first icon passed is taken and any additional arguments are ignored.
            \\
        , .{});
        std.process.exit(1);
    };

    const icon_size = if (std.os.argv.len <= 2)
        32
    else
        std.fmt.parseInt(i32, std.mem.span(std.os.argv[2]), 10) catch |err| switch (err) {
            error.InvalidCharacter => {
                print("Given icon size is not a valid integer\n", .{});
                std.process.exit(1);
            },
            error.Overflow => {
                print("Integer overflow.\n", .{});
                std.process.exit(1);
            },
        };

    const icon_theme = cgtk.gtk_icon_theme_get_default();
    const icon_info = cgtk.gtk_icon_theme_lookup_icon(icon_theme, icon_name, icon_size, cgtk.G_APPLICATION_FLAGS_NONE);
    const icon_path = if (icon_info != null)
        std.mem.span(cgtk.gtk_icon_info_get_filename(icon_info))
    else
        "";
    if (!std.mem.eql(u8, icon_path, "")) {
        stdout.print("{s}\n", .{icon_path}) catch unreachable;
        std.process.exit(0);
    } else {
        print("No icon found\n", .{});
        std.process.exit(1);
    }
}
