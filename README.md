# GTK3 Named Icon

Get system icons from the name using the default icon theme.

## Usage

```sh
$ namedicon
No icon requested

Usage: namedicon <icon-name> [<size>]
  <icon-name>     : The name of the icon you want.
  <size> Optional : The size of the icon you want. Defaults to 32.

Only the first icon passed is taken and any additional arguments are ignored.
$ namedicon system-shutdown
/usr/share/icons/breeze/actions/32/system-shutdown.svg
```

## Building

Requires `zig` to be installed.

```sh
zig build --release=safe
```

Copy the resulting binary from `./zig-out/bin/namedicon`.

## Packages

- AUR: [namedicon-git](https://aur.archlinux.org/packages/namedicon-git)
